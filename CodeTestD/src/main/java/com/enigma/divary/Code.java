package com.enigma.divary;

public class Code {

    public int solution(Integer[] a) {

        int test = -1;

        while (true) {

            for (int i = 1; i <= a.length; i++){

                if (test == a[i-1]) break;
                else if (i == a.length) return test;
            }
            test--;
        }
    }
}
