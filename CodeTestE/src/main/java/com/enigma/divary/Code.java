package com.enigma.divary;

public class Code {

    public String solution(String name) {

        String[] word = name.split(" ");
        String nama = "";

        for (int i = 0; i < word.length; i++){

            if (word[i].length() <= 2){ nama += word[i];
            } else {
                StringBuilder ret = new StringBuilder();
                ret.append(word[i].charAt(0));
                for (int j = 1; j < word[i].length() - 1; j++) {
                    ret.append("*");
                }

                ret.append(word[i].charAt(word[i].length() - 1));
                nama += ret.toString();
            }

            if (word.length != i+1) nama += " ";
        }
        return nama;
    }
}
