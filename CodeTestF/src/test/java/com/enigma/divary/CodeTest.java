package com.enigma.divary;

import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CodeTest {

    @Test
    void solution() {

        Code code = new Code();

        HashMap<String, Integer> tests = new HashMap<String, Integer>();

        tests.put("sisayang", 10);
        tests.put("pulxu pqnjzng", 0);
        tests.put("Pulau Punjung", 1);

        tests.forEach((original, masked) -> assertEquals(code.solution(original).size(), masked));
    }
}